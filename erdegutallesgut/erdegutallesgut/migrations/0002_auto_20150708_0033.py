# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('erdegutallesgut', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HolidayType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name der Urlaubsart')),
            ],
            options={
                'verbose_name': 'Urlaubsart',
                'verbose_name_plural': 'Urlaubsarten',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='diaryentry',
            options={'ordering': ['-date'], 'verbose_name': 'Tagebuch Eintrag', 'verbose_name_plural': 'Tagebuch Eintr\xe4ge'},
        ),
        migrations.AlterModelOptions(
            name='holiday',
            options={'verbose_name': 'Urlaub', 'verbose_name_plural': 'Urlaube'},
        ),
        migrations.AlterModelOptions(
            name='settings',
            options={'verbose_name': 'Allgemeine Einstellungen', 'verbose_name_plural': 'Allgemeine Einstellungen'},
        ),
        migrations.AddField(
            model_name='holiday',
            name='holiday_type',
            field=models.ForeignKey(default=1, to='erdegutallesgut.HolidayType'),
            preserve_default=False,
        ),
    ]
