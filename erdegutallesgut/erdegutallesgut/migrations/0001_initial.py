# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import thumbnailfield.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CarouselImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', thumbnailfield.fields.ThumbnailField(help_text=b'Hintergrund-Bilder das auf der websize angezeigt wird', max_length=255, upload_to=b'background_images', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DiaryEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Der Titel eines Tagebucheintrages wird als \xc3\xbcberschrift f\xc3\xbcr den den Tagbucheintrag verwendet.', max_length=255, verbose_name=b'Titel', blank=True)),
                ('date', models.DateField(help_text=b'Datum an dem der Tagebucheintrag ver\xc3\xb6ffentlicht wurde. Sollte das Datum in der Zukunft liegen, wird der Tagebucheintrag erst angezeigt so bald das Eingegebene Datum erricht wurde.', verbose_name=b'Datum')),
                ('entry', ckeditor.fields.RichTextField(verbose_name=b'Eintrags Text, geben Sie hier den Text des Tagebucheintrages ein.')),
                ('active', models.BooleanField(default=True, help_text=b'Nur als aktiv markierte Tagebucheintr\xc3\xa4ge werden auf der Homepage angezeigt.', verbose_name=b'Aktiv')),
            ],
            options={
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Holiday',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('begin', models.DateField(help_text=b'Das Datum an dem dieser Betriebsurlaub beginnt.', verbose_name=b'Anfangsdatum')),
                ('end', models.DateField(help_text=b'Das Datum an dem dieser Betriebsurlaub endet.', verbose_name=b'Enddatum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Dieser Name wird in der Produkt\xc3\xbcbersicht als \xc3\x9cberschrift verwendet.', max_length=255, verbose_name=b'Produktname', blank=True)),
                ('image', thumbnailfield.fields.ThumbnailField(max_length=255, upload_to=b'product_images', blank=True)),
                ('description', ckeditor.fields.RichTextField()),
                ('special_offer', models.BooleanField(default=False, help_text=b'Spezialangebote werden auf der Hompage besonders hervorgehoben.', verbose_name=b'Spezial Angebot')),
                ('active', models.BooleanField(default=True, help_text=b'Nur als aktiv markierte Produkte werden auf der Homepage angezeigt.', verbose_name=b'Aktiv')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_line1', models.CharField(help_text=b'Stra\xc3\x9fe/Gasse, Hausnummer/T\xc3\xbcrnummer. Wird auf jeder Seite der Homepage angezeigt.', max_length=255, verbose_name=b'Adresszeile 1')),
                ('address_line2', models.CharField(help_text=b'Postleitzahl und Ort. Wird auf jeder Seite der Homepage angezeigt.', max_length=255, verbose_name=b'Adresszeile 2')),
                ('address_line3', models.CharField(help_text=b'optional. Falls ein Adresszusatz ben\xc3\xb6tigt wird.', max_length=255, verbose_name=b'Adresszeile 3', blank=True)),
                ('open_hours_line1', models.CharField(help_text=b'Bitte geben sie hier die \xc3\x96ffnungszeiten ein. TIPP: Achten Sie dabei darauf, dass die \xc3\x96ffnungszeiten durch die Aufteilung auf mehrere Zeilen auf der Homepage sch\xc3\xb6n angeordnet wirken.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 1')),
                ('open_hours_line2', models.CharField(help_text=b'optional. F\xc3\xbcr weitere \xc3\x96ffnungszeitenangaben.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 2', blank=True)),
                ('open_hours_line3', models.CharField(help_text=b'optional. F\xc3\xbcr weitere \xc3\x96ffnungszeitenangaben.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 3', blank=True)),
                ('open_hours_line4', models.CharField(help_text=b'optional. F\xc3\xbcr weitere \xc3\x96ffnungszeitenangaben.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 4', blank=True)),
                ('open_hours_line5', models.CharField(help_text=b'optional. F\xc3\xbcr weitere \xc3\x96ffnungszeitenangaben.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 5', blank=True)),
                ('open_hours_line6', models.CharField(help_text=b'optional. F\xc3\xbcr weitere \xc3\x96ffnungszeitenangaben.', max_length=255, verbose_name=b'\xc3\x96ffnungszeiten Zeile 6', blank=True)),
                ('phone', models.CharField(help_text=b'Diese Telefonnummer wird Ihren Kunden neben Ihrer Addresse angezeigt.', max_length=255, verbose_name=b'Telefon Nummer', blank=True)),
                ('email', models.CharField(help_text=b'Diese Email-Adresse wird Ihren Kunden gleich neben Ihrer Adresse angezeigt.', max_length=255, verbose_name=b'Email-Adresse', blank=True)),
                ('copyright_notice', models.CharField(help_text=b'Diese Copyright Klause wird auf jeder Seite angezeigt um Ihnen Ihre Recht an Ihren Seiten-Inhalten auch in anderen Rechtsordnungen zu sichern. Au\xc3\x9ferdem schafft dies Klarheit f\xc3\xbcr Ihre Kunden, in welcher Weise die Inhalte Ihrer Homepage verwendet werden d\xc3\xbcrfen.', max_length=255, verbose_name=b'Copyright Klause', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SiteBackgroundImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(help_text=b'Dieses Bild wird als eines der Hintergrundbilder auf der Webseite angezeigt.', max_length=255, upload_to=b'background_images', blank=True)),
                ('settings', models.ForeignKey(to='erdegutallesgut.Settings')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='carouselimage',
            name='settings',
            field=models.ForeignKey(to='erdegutallesgut.Settings'),
            preserve_default=True,
        ),
    ]
