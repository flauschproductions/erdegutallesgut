# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from django_migration_fixture import fixture
import erdegutallesgut


class Migration(migrations.Migration):

    dependencies = [
        ('erdegutallesgut', '0002_auto_20150708_0033'),
    ]

    operations = [
        migrations.RunPython(**fixture(erdegutallesgut, ['initial_data.json'])),

    ]
