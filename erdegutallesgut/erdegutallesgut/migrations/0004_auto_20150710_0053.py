# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('erdegutallesgut', '0003_auto_20150709_1112'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='carouselimage',
            options={'verbose_name': 'Karussell Bild', 'verbose_name_plural': 'Karussell Bilder'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': 'Produkt', 'verbose_name_plural': 'Produkte'},
        ),
        migrations.AlterModelOptions(
            name='sitebackgroundimage',
            options={'verbose_name': 'Seiten Hintergrund Bild', 'verbose_name_plural': 'Seiten Hintergrund Bilder'},
        ),
    ]
