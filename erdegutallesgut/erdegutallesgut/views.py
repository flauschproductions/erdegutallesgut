from django.shortcuts import render, render_to_response
from django.template import RequestContext
import datetime

from erdegutallesgut.models import Product
from erdegutallesgut.models import DiaryEntry

# Create your views here.

def products(request):
  d = {'products': Product.objects.filter(special_offer=False)}
  return render_to_response('products.html', RequestContext(request, d))

def special_offers(request):
  d = {'products': Product.objects.filter(special_offer=True)}
  return render_to_response("special_offers.html", RequestContext(request, d))

def diary(request):
  d = {'diary_entries': DiaryEntry.objects.filter(active=True, date__lte=datetime.date.today())}
  return render_to_response('diary.html', RequestContext(request, d))
