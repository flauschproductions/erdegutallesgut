from django import template
from datetime import datetime

#from django.template.defaultfilters import stringfilter

from erdegutallesgut.models import SiteBackgroundImage
from erdegutallesgut.models import CarouselImage
from erdegutallesgut.models import Settings
from erdegutallesgut.models import Holiday

register = template.Library()

class ShopHoursNode(template.Node):
  def __init__(self):
    pass
  def render(self,context):
    try:
      ret = ''
      s = Settings.objects.first()
      if s != None:
        ret += '<li>'+s.open_hours_line1+'</li>' if s.open_hours_line1 != None and s.open_hours_line1 != '' else ''
        ret += '<li>'+s.open_hours_line2+'</li>' if s.open_hours_line2 != None and s.open_hours_line2 != '' else ''
        ret += '<li>'+s.open_hours_line3+'</li>' if s.open_hours_line3 != None and s.open_hours_line3 != '' else ''
        ret += '<li>'+s.open_hours_line4+'</li>' if s.open_hours_line4 != None and s.open_hours_line4 != '' else ''
        ret += '<li>'+s.open_hours_line5+'</li>' if s.open_hours_line5 != None and s.open_hours_line5 != '' else ''
        ret += '<li>'+s.open_hours_line6+'</li>' if s.open_hours_line6 != None and s.open_hours_line6 != '' else ''
      return ret
    except ValueError:
      return ''

@register.tag
def egag_shop_hours(parser, token):
  syntax_message = "egag_shop_hours expects a syntax of: "+\
                   "egag_shop_hours"
  bits = token.split_contents()
  if len(bits) != 1:
    raise template.TemplateSyntaxError(syntax_message)
  return ShopHoursNode()

class EgagAddressNode(template.Node):
  def __init__(self):
    pass
  def render(self,context):
    try:
      s = Settings.objects.first()
      ret = '<br />'.join(filter(lambda x: len(x)>0,[s.address_line1,s.address_line2,s.address_line3]))
      ret += '<br /><a href="tel:'+s.phone+'">'+s.phone+'</a>' if s.phone != None and s.phone != '' else ''
      ret += '<br /><a href="mailto:'+s.email+'">'+s.email+'</a>' if s.email != None and s.email != '' else ''
      return ret
    except ValueError:
      return ''

@register.tag
def egag_address(parser, token):
  syntax_message = "egag_address expects a syntax of: "+\
                   "{% egag_address %}"
  bits = token.split_contents()
  if len(bits) != 1:
    raise template.TemplateSyntaxError(syntax_message)
  return EgagAddressNode()

class EgagCopyrightNoticeNode(template.Node):
  def __init__(self):
    pass
  def render(self,context):
    try:
      s = Settings.objects.first()
      return s.copyright_notice
    except ValueError:
      return ''

@register.tag
def egag_copyright_notice(parser, token):
  syntax_message = "egag_copyright_notice expects a syntax of: "+\
                   "{% egag_copyright_notice %}"
  bits = token.split_contents()
  if len(bits) != 1:
    raise template.TemplateSyntaxError(syntax_message)
  return EgagCopyrightNoticeNode()

class EgagHolidayWarningNode(template.Node):
  def __init__(self):
    pass
  def render(self,context):
    try:
      # get next holiday where end date > now
      holidays = Holiday.objects.filter(end__gte=datetime.now()).order_by('begin')
      if len(holidays) > 0:

        h = holidays[0]
        html_class = 'holidaymessage' if h.begin > datetime.date(datetime.now()) else 'holidaymessagenow'

        if h.begin == h.end:
          if h.holiday_type.name == "Betriebsurlaub":
            return '<p class="%s">Wir haben am <strong>%02d.%02d.%04d</strong> Betriebsurlaub.</p>' % \
              (html_class, h.begin.day, h.begin.month, h.begin.year)
          else:
            return '<p class="%s">Wir haben am <strong>%02d.%02d.%04d</strong> geschlossen.</p>' % \
              (html_class, h.begin.day, h.begin.month, h.begin.year)
        else:
          if h.holiday_type.name == "Betriebsurlaub":
            return '<p class="%s">Wir haben vom <strong>%02d.%02d.%04d</strong> bis zum<br /><strong>%02d.%02d.%04d</strong> Betriebsurlaub.</p>' % \
              (html_class, h.begin.day, h.begin.month, h.begin.year, h.end.day, h.end.month, h.end.year)
          else:
            return '<p class="%s">Wir haben vom <strong>%02d.%02d.%04d</strong> bis zum<br /><strong>%02d.%02d.%04d</strong> geschlossen.</p>' % \
              (html_class, h.begin.day, h.begin.month, h.begin.year, h.end.day, h.end.month, h.end.year)

      else:
        return ''
    except ValueError:
      return ''
    

@register.tag
def egag_holiday_warning(parser,token):
  syntax_message = "egag_holiday_warning expects a syntax of: "+\
                   "{% egag_holiday_warning %}"
  bits = token.split_contents()
  if len(bits) != 1:
    raise template.TemplatesSyntaxError(syntax_message)
  return EgagHolidayWarningNode()



class EgagRndBgImgUrl(template.Node):
  def __init__(self):
    pass
  def render(self,context):
    try:
      i = SiteBackgroundImage.objects.all().order_by('?')[0]
      #print dir(i.image.thumbnail.url_480x360)
      return i.image.url
    except ValueError:
      return ''

@register.tag
def egag_rnd_bg_img_url(parser,token):
  syntax_message = "egag_rnd_bg_img_url expects a syntax of: "+\
                   "{% egag_rnd_bg_img_url %}"
  bits = token.split_contents()
  if len(bits) != 1:
    raise template.TemplatesSyntaxError(syntax_message)
  return EgagRndBgImgUrl()



@register.assignment_tag
def egag_get_carousel_images():
  return CarouselImage.objects.all()

@register.filter
def egag_as_fraction_of_100(number):
  return "%.2f" % (100. / number) 
