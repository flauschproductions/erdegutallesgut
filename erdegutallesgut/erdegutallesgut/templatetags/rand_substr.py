from django import template
from random import randint

from django.template.defaultfilters import stringfilter


register = template.Library()

#class RandomBgImgNode(template.Node):
#  def __init__(self):
#    pass
#  def render(self,context):
#    imgs = ['066.jpg', '067.jpg', '171.jpg', '173.jpg', '179.jpg', '196.jpg', '199.jpg']
#    return imgs[randint(0,size(imgs)-1)]
#
#@register.tag
#def random_bg_img(parser, token):
#  syntax_message = "sud_favicon_url expects a syntax of: "+\
#                   "random_bg_img"
#  bits = token.split_contents()
#  if len(bits) != 1:
#    raise template.TemplateSyntaxError(syntax_message)
#  return RandomBgImgNode()

@register.filter(is_safe=True)
@stringfilter
def rand_substr(s):
  strngs = s.split(' ')
  return strngs[randint(0,len(strngs)-1)]
