"""
Django settings for erdegutallesgut project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# check weather there we're on a deployment or on a dev system.

try:
  """ Try to obtain server settings if present """

  exec open('srv_settings.py') in globals()

except IOError:
  """ Since server settings are not presend
      here goes the development configuration."""

  # SECURITY WARNING: keep the secret key used in production secret!
  SECRET_KEY = '0m85o7&=6wcuiyub67!)3#czuy+-w+oh+4mfjst@0=_2#-15el'

  # SECURITY WARNING: don't run with debug turned on in production!
  DEBUG = True
  TEMPLATE_DEBUG = True

  # make application server serve static and media content
  SERVE_FILES = True

  ALLOWED_HOSTS = []

  # Database
  # https://docs.djangoproject.com/en/1.6/ref/settings/#databases

  DATABASES = {
    'default': {
      'ENGINE': 'django.db.backends.sqlite3',
      'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
  }

 

SITE_ID = 1


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/



# Application definition

INSTALLED_APPS = (
  'grappelli',
  'django.contrib.admin',
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.messages',
  'django.contrib.staticfiles',

  'django.contrib.webdesign',
  'django.contrib.sites',
  'django.contrib.flatpages',

  'django_migration_fixture',
  'gunicorn',
  'ckeditor',
  'extended_flatpages',
  #'external_migrations.extended_flatpages',
  'thumbnailfield',

  'erdegutallesgut',
)

MIDDLEWARE_CLASSES = (
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',
  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',
  'django.middleware.clickjacking.XFrameOptionsMiddleware',

  'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

ROOT_URLCONF = 'erdegutallesgut.urls'

WSGI_APPLICATION = 'erdegutallesgut.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'de-at'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR,'static_erdegutallesgut')

# Media files (User Uploads)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR,'media_erdegutallesgut')

## strange logging setting required by gunicorn

LOGGING = {
  'version': 1,
}

# grappelli dependencies

from django.conf import global_settings
TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
  "django.core.context_processors.request",
)

# ckeditor

CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT,'dyn')
try:
  os.makedirs(CKEDITOR_UPLOAD_PATH)
except OSError as e:
  if e.errno != 17: # check for file exists error
    raise e

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
                    [ 'Source','-','Save','-', 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo', '-', 'Find','Replace','-','SelectAll'],
                    [ 'NumberedList','BulletedList','-','Outdent','Indent'], '/',
                    [ 'Format' ], [ 'Bold','Italic','Underline','-','RemoveFormat' ] ,
                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
                    [ 'Link','Unlink' ],
                ],
        'format_tags': 'p;h1;h2;h3;h4;h5;h6;pre;address',
    },
}
