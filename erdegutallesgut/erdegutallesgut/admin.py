from django.contrib import admin

from erdegutallesgut.models import Settings
from erdegutallesgut.models import SiteBackgroundImage
from erdegutallesgut.models import CarouselImage
from erdegutallesgut.models import Product
from erdegutallesgut.models import DiaryEntry
from erdegutallesgut.models import Holiday
from erdegutallesgut.models import HolidayType

# Register your models here.


class ProductAdmin(admin.ModelAdmin):
  list_display = ('name','special_offer')
admin.site.register(Product,ProductAdmin)

class DiaryAdmin(admin.ModelAdmin):
  list_display = ('active','title','date')
admin.site.register(DiaryEntry,DiaryAdmin)

class SiteBackgroundImageAdmin(admin.TabularInline):
  model = SiteBackgroundImage

class CarouselImageAdmin(admin.TabularInline):
  model = CarouselImage

class SettingsAdmin(admin.ModelAdmin):
  inlines = [
    SiteBackgroundImageAdmin,
    CarouselImageAdmin,
  ]
admin.site.register(Settings,SettingsAdmin)

class HolidayAdmin(admin.ModelAdmin):
  list_display = ('begin', 'end')
admin.site.register(Holiday,HolidayAdmin)

class HolidayTypeAdmin(admin.ModelAdmin):
  pass
admin.site.register(HolidayType, HolidayTypeAdmin)
