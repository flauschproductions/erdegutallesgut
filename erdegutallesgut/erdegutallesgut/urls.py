from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from erdegutallesgut import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
  # Examples:
  # url(r'^$', 'erdegutallesgut.views.home', name='home'),
  # url(r'^blog/', include('blog.urls')),

  url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
  url(r'^admin/', include(admin.site.urls)),

  url(r'^produkte/', 'erdegutallesgut.views.products'),
  url(r'^spezialangebote/', 'erdegutallesgut.views.special_offers'),
  url(r'^tagebuch/', 'erdegutallesgut.views.diary'),

  url(r'^ckeditor/', include('erdegutallesgut.urls_ckeditor_workaround')),
)



# make application-server serve static and media content if configured so

if settings.SERVE_FILES:
  urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
      'document_root': settings.MEDIA_ROOT,
    }),
  )

if settings.SERVE_FILES:
  urlpatterns += patterns('',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
      'document_root': settings.STATIC_ROOT,
    }),
  )
