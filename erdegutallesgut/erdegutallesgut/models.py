#! /usr/bin/evn python
# -*- coding: utf8 -*-

from django.db import models

from ckeditor.fields import RichTextField
from thumbnailfield.fields import ThumbnailField

# Create your models here.


class Product(models.Model):
  name = models.CharField('Produktname',max_length=255,blank=True,help_text="Dieser Name wird in der Produktübersicht als Überschrift verwendet.")
  image = ThumbnailField(max_length=255,blank=True,upload_to='product_images',
    patterns={
      'micro': (16,9),
    }
  )
  description = RichTextField()
  special_offer = models.BooleanField('Spezial Angebot', default=False, help_text="Spezialangebote werden auf der Hompage besonders hervorgehoben.")
  active = models.BooleanField('Aktiv', default=True, help_text='Nur als aktiv markierte Produkte werden auf der Homepage angezeigt.')
  def __unicode__(self):
    return self.name
  class Meta:
    verbose_name = "Produkt"
    verbose_name_plural = "Produkte"

class DiaryEntry(models.Model):
  title = models.CharField('Titel',max_length=255,blank=True,help_text='Der Titel eines Tagebucheintrages wird als überschrift für den den Tagbucheintrag verwendet.')
  date = models.DateField('Datum',help_text='Datum an dem der Tagebucheintrag veröffentlicht wurde. Sollte das Datum in der Zukunft liegen, wird der Tagebucheintrag erst angezeigt so bald das Eingegebene Datum erricht wurde.')
  entry = RichTextField('Eintrags Text, geben Sie hier den Text des Tagebucheintrages ein.')
  #image = models.ImageField(max_length=255,blank=True,upload_to='diary_images',help_text="optional. Bild das bei dem Tagebucheintrag angezeigt wird.")
  active = models.BooleanField('Aktiv', default=True, help_text="Nur als aktiv markierte Tagebucheinträge werden auf der Homepage angezeigt.")
  def __unicode__(self):
    return self.title
  class Meta:
    ordering = ['-date']
    verbose_name = "Tagebuch Eintrag"
    verbose_name_plural = "Tagebuch Einträge"


class SiteBackgroundImage(models.Model):
  image = models.ImageField(max_length=255,blank=True,upload_to='background_images',
    help_text='Dieses Bild wird als eines der Hintergrundbilder auf der Webseite angezeigt.'
  )
  settings = models.ForeignKey('Settings')
  def __unicode__(self):
    return str(self.image)
  class Meta:
    verbose_name = "Seiten Hintergrund Bild"
    verbose_name_plural = "Seiten Hintergrund Bilder"

class CarouselImage(models.Model):
  image = ThumbnailField(max_length=255,blank=True,upload_to='background_images',
    patterns={'sidebar':   (640, 360, 'resize')},
    help_text='Hintergrund-Bilder das auf der websize angezeigt wird'
  )
  settings = models.ForeignKey('Settings')
  def __unicode__(self):
    return str(self.image)
  class Meta:
    verbose_name = "Karussell Bild"
    verbose_name_plural= "Karussell Bilder"


class Settings(models.Model):
  address_line1 = models.CharField('Adresszeile 1', max_length=255, help_text="Straße/Gasse, Hausnummer/Türnummer. Wird auf jeder Seite der Homepage angezeigt.")
  address_line2 = models.CharField('Adresszeile 2', max_length=255, help_text="Postleitzahl und Ort. Wird auf jeder Seite der Homepage angezeigt.")
  address_line3 = models.CharField('Adresszeile 3', max_length=255, blank=True, help_text="optional. Falls ein Adresszusatz benötigt wird.")

  open_hours_line1 = models.CharField('Öffnungszeiten Zeile 1', max_length=255, help_text='Bitte geben sie hier die Öffnungszeiten ein. TIPP: Achten Sie dabei darauf, dass die Öffnungszeiten durch die Aufteilung auf mehrere Zeilen auf der Homepage schön angeordnet wirken.')
  open_hours_line2 = models.CharField('Öffnungszeiten Zeile 2', max_length=255, blank=True, help_text="optional. Für weitere Öffnungszeitenangaben.")
  open_hours_line3 = models.CharField('Öffnungszeiten Zeile 3', max_length=255, blank=True, help_text="optional. Für weitere Öffnungszeitenangaben.")
  open_hours_line4 = models.CharField('Öffnungszeiten Zeile 4', max_length=255, blank=True, help_text="optional. Für weitere Öffnungszeitenangaben.")
  open_hours_line5 = models.CharField('Öffnungszeiten Zeile 5', max_length=255, blank=True, help_text="optional. Für weitere Öffnungszeitenangaben.")
  open_hours_line6 = models.CharField('Öffnungszeiten Zeile 6', max_length=255, blank=True, help_text="optional. Für weitere Öffnungszeitenangaben.")

  phone = models.CharField('Telefon Nummer', max_length=255, blank=True, help_text='Diese Telefonnummer wird Ihren Kunden neben Ihrer Addresse angezeigt.')
  email = models.CharField('Email-Adresse', max_length=255, blank=True, help_text='Diese Email-Adresse wird Ihren Kunden gleich neben Ihrer Adresse angezeigt.')

  copyright_notice = models.CharField('Copyright Klause', max_length=255, blank=True, help_text="Diese Copyright Klause wird auf jeder Seite angezeigt um Ihnen Ihre Recht an Ihren Seiten-Inhalten auch in anderen Rechtsordnungen zu sichern. Außerdem schafft dies Klarheit für Ihre Kunden, in welcher Weise die Inhalte Ihrer Homepage verwendet werden dürfen.")

  def __unicode__(self):
    return u', '.join((self.address_line1,self.address_line2,self.address_line3))
  class Meta():
    verbose_name = "Allgemeine Einstellungen"
    verbose_name_plural = "Allgemeine Einstellungen"

class Holiday(models.Model):
  begin = models.DateField('Anfangsdatum', help_text="Das Datum an dem dieser Betriebsurlaub beginnt.")
  end = models.DateField('Enddatum', help_text="Das Datum an dem dieser Betriebsurlaub endet.")
  holiday_type = models.ForeignKey('HolidayType')
  def __unicode__(self):
    return str(self.begin) + ' - ' + str(self.end)
  class Meta():
    verbose_name = "Urlaub"
    verbose_name_plural = "Urlaube"

class HolidayType(models.Model):
  name = models.CharField('Name der Urlaubsart', max_length=255)
  def __unicode__(self):
    return str(self.name)
  class Meta():
    verbose_name = "Urlaubsart"
    verbose_name_plural = "Urlaubsarten" 
