# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CMSFlatPage',
            fields=[
                ('flatpage_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='flatpages.FlatPage')),
                ('description', models.CharField(max_length=100, verbose_name='Description')),
                ('keywords', models.CharField(max_length=255, verbose_name='Keywords')),
            ],
            options={
            },
            bases=('flatpages.flatpage',),
        ),
    ]
